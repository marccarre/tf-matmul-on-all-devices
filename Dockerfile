FROM tensorflow/tensorflow:latest-gpu
WORKDIR /
COPY matmul.py /
ENTRYPOINT ["python", "-u", "matmul.py"]
