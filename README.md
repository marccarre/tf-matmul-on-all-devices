# TensorFlow matrices multiplication on all devices

## Why?

Convenient to test a Kubernetes cluster with GPUs.

## How to build it?

```bash
$ docker build -t marccarre/tf-matmul-on-all-devices:08d167861fba .
```

### How to release it?

```bash
$ docker login
$ docker push marccarre/tf-matmul-on-all-devices:08d167861fba
```

### How to run it?

```bash
### Create your GKE cluster:
$ gcloud beta container clusters create gke-with-gpus \
    --accelerator type=nvidia-tesla-k80,count=1 \
    --zone us-central1-a --cluster-version 1.10.2-gke.3

### Configure kubectl to point to your GKE cluster:
$ gcloud container clusters get-credentials gke-with-gpus --zone=us-central1-a

### Install NVIDIA GPU device drivers:
$ kubectl apply -f https://raw.githubusercontent.com/GoogleCloudPlatform/container-engine-accelerators/stable/nvidia-driver-installer/cos/daemonset-preloaded.yaml

### Run test workload:
$ cat <<EOF | kubectl apply -f -
---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: tf-matmul-on-all-devices
spec:
  replicas: 2
  template:
    metadata:
      labels:
        name: tf-matmul-on-all-devices
    spec:
      containers:
      - name: tf-matmul-on-all-devices
        image: "marccarre/tf-matmul-on-all-devices:08d167861fba"
        args:
        # Number of iterations:
        - '1000'
        # Minimum matrix size:
        - '5000'
        # Maximum matrix size:
        - '8000'
        # Sleeping time, in seconds, between batches on one given device (e.g. CPU vs. GPU):
        - '60'
        # Only use GPUs:
        - 'gpu-only'
        resources:
          limits:
            nvidia.com/gpu: 1
        ports:
        - containerPort: 8888
---
apiVersion: v1
kind: Service
metadata:
  name: tf-matmul-on-all-devices
spec:
  ports:
    - port: 8888
  selector:
    name: tf-matmul-on-all-devices
EOF
```
