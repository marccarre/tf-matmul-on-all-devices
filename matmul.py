import logging
import random
import sys
import time
import tensorflow as tf
from tensorflow.python.client import device_lib


def get_logger(name):
    ''' Create and configure a logger for the provided module or name.'''
    logger = logging.getLogger(name)
    handler = logging.StreamHandler()
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)
    return logger


def main(num_iterations, min_size, max_size, sleeping_time_in_secs, gpu_only):
    logger = get_logger('matmul')

    # Make TensorFlow only allocate the amount of memory it needs, instead of
    # trying to allocate all of the GPU memory, as this makes for a visually
    # interesting chart when looking at the corresponding metric.
    # See also: https://www.tensorflow.org/programmers_guide/using_gpu#allowing_gpu_memory_growth
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True

    # Run eagerly, instead of creating a graph:
    tf.enable_eager_execution(config=config)

    while True:
        for device in device_lib.list_local_devices():
            device_name = str(device.name.decode('utf-8'))  # Convert from unicode to str
            if gpu_only and 'gpu' not in device_name.lower():
                logger.info('skipping %s' % device_name)
                continue
            logger.info('multiplying using: %s' % device_name)
            with tf.device(device_name):
                size = random.randint(min_size, max_size)
                a = tf.random_normal([size, size], name='a')
                b = tf.random_normal([size, size], name='b')
                for x in range(num_iterations):
                    logger.info('iteration #%s/%s: multiplying two %s x %s matrices using %s' % (x+1, num_iterations, size, size, device_name))
                    a = tf.matmul(a, b)
            time.sleep(sleeping_time_in_secs)

if __name__ == '__main__':
    num_iterations, min_size, max_size, sleeping_time_in_secs = [int(x) for x in sys.argv[1:5]]
    gpu_only = 'gpu-only' in sys.argv[5]
    main(num_iterations, min_size, max_size, sleeping_time_in_secs, gpu_only)
